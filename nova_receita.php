<?php
	require_once('assets/inc-php/func.php');
	if(isset($_SESSION['id'])){ //só pra ver se tem alguém logado
?>
<!DOCTYPE html>
<html>
<head>
	<title>REAL MEAL - Nova receita</title>
	<script type="text/javascript" src="assets/js/script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="./assets/css/style.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="index.php">
				<img src="assets/img/real_meal.png" >
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="alimentos.php">Alimentos<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="perfil.php">Perfil<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="dash.php">Dash<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<form name="formsair" action="assets/inc-php/func.php" method="post">
							<input type="submit" class="btn btn-outline-secondary btn-sm" value="Sair" name="sair">
						</form>
					</li>				
				</ul>
			</div>
		</nav>
		<div class="jumbotron" align="center">
			<div class="card" style="width: 50rem;">
				<h4 class="card-header">Nova receita</h4>
				<div class="card-body">
					<form name="form_receita" action="nova_receita.php" method="post" enctype="multipart/form-data">
						<div class="form-group row">
							<label for="inputPassword" class="col-sm-2 col-form-label">Título</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="titulo" name="titulo">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputPassword" class="col-sm-2 col-form-label">Descrição</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="descricao" name="descricao" rows="3"></textarea>
							</div>
						</div>
						<!-- <div class="form-group row">
							<label for="inputPassword" class="col-sm-2 col-form-label">Kcal</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" id="kcal" name="kcal">
							</div>
						</div> -->
						<h6>Categoria</h6>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="categoria1" id="categoria" value="1" >
							<label class="form-check-label">Vegana</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="categoria2"  value="2">
							<label class="form-check-label">Vegetariana</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="categoria3"  value="3">
							<label class="form-check-label">Sem lactose</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="categoria4"  value="4" >
							<label class="form-check-label">Sem gluten</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="categoria5"  value="5">
							<label class="form-check-label">Sem açúcar</label>
						</div>
						<p> <br> </p>
						<div class="row">
							<div class="col col-12 col-md-6 offset-md-3">
								<div class="form-group">
									<label class="custom-file">
										<span class="fake-button">
											Arquivinhooo:  <input type="file" name="arquivo">
									</label>
										</span>
										<!-- <input type="hidden" name ="MAX_FILE_SIZE" value="200000"> -->

								</div>
								<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pronto!" name="envia_receita">
					</form>
				</div>	
			</div>
		</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>		
	</body>
</html>	

<?php
}else{
	header('Location: index.php');
}
?>