<?php
require_once('assets/inc-php/func.php');
if(isset($_SESSION['id'])){
	?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar user</title>
	<script type="text/javascript" src="assets/js/script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="index.php">
			<img src="img/real_meal.png" >
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Receitas<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="#">Alimentos<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="perfil.php">Perfil<span class="sr-only">(current)</span></a>
				</li>			
			</ul>
		</div>
	</nav>

	<div class="jumbotron" align="center">
		<div class="card" style="width: 50rem;">
			<h4 class="card-header">Novo usuário</h4>
			<div class="card-body">
				<form name="edit_perfil" action="editar_perfil.php" method="post">
					Nome:
					<input type="text" name="nome" value="<?php echo $_SESSION['nome']; ?>">
					<br>
					<br>
					Senha atual:
					<input type="password" name="senhatual" placeholder="******">
					<br>
					<br>
					Escolha uma nova senha:
					<input type="password" name="senha1" placeholder="******">
					<br>
					<br>
					Confirme sua senha:
					<input type="password" name="senha2" placeholder="******">
					<div class="form-group" align="center">
						<label for="exampleFormControlFile1">Escolha uma nova foto de perfil</label>
						<input type="file" class="form-control-file" id="exampleFormControlFile1">
					</div>
					<input type="submit" class="btn btn-primary" value="Pronto!" name="edita">
				</form>
			</div>	
		</div>
	</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
<?php
}else{
	header('Location: index.php');
}
?>