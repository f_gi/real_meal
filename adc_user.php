<?php
require_once('assets/inc-php/func.php');
?>

<!DOCTYPE html>
<html>
<head>

	<script type="text/javascript" src="func.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">	
	<title>Real Meal | Novo usuário</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="index.php">
			<img src="assets/img/real_meal.png" >
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Alimentos<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="login.php">Login<span class="sr-only"></span></a>
				</li>				
			</ul>
		</div>
	</nav>
	<div class="jumbotron" align="center">
		<div class="card" style="width: 50rem;">
			<h4 class="card-header">Novo usuário</h4>
			<div class="card-body">
				<form name="formuser" action="adc_user.php" method="post">
					Informe seu nome:
					<input type="text" name="nome" placeholder="Seu nome aqui">
					<br>
					<br>
					Informe seu e-mail:
					<input type="email" name="email" placeholder="exemple@exemple.com">
					<br>
					<br>
					Escolha uma senha:
					<input type="password" name="senha1" placeholder="******">
					<br>
					<br>
					Confirme sua senha:
					<input type="password" name="senha2" placeholder="******">
					<div class="form-group" align="center">
						<label for="exampleFormControlFile1">Escolha uma foto de perfil</label>
						<input type="file" class="form-control-file" id="exampleFormControlFile1">
					</div>
					<input type="submit" class="btn btn-primary" value="Criar" name="cria" onclick="return validauser();">
				</form>
			</div>	
		</div>
	</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>