<?php
	require_once('assets/inc-php/func.php');  
	require_once('assets/inc-php/func.php');
	$id_receita=$_SESSION['id_receita'];
	$conexao = pg_connect('host=localhost port=5432 dbname=daw user=postgres password=pastel');
	$sql="SELECT * FROM receitas WHERE id=$id_receita";
	$resultado = pg_query($conexao, $sql);
	$resultado_array = pg_fetch_all($resultado);
 ?>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="assets/js/script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">	
	<title>Real Meal | Editar receita</title>
</head>
	<body>
	<?php if(isset($_SESSION['id'])) { //O ERRO TA NESSE IF SOCORRO DEUS ?>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="index.php">
				<img src="assets/img/real_meal.png" >
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="alimentos.php">Alimentos<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="perfil.php">Perfil<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="dash.php">Dash<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item active">
						<form name="formsair" action="assets/inc-php/func.php" method="post">
							<input type="submit" class="btn btn-outline-secondary btn-sm" value="Sair" name="sair">
						</form>
					</li>				
				</ul>
			</div>
		</nav>
		<div class="jumbotron" align="center">
			<div class="card" style="width: 50rem;">
				<h4 class="card-header">Editar Receita</h4>
				<div class="card-body">
					<form name="form_receita" action="nova_receita.php" method="post">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Tiítulo</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="titulo" value="<?php echo $resultado_array[0]['titulo']; ?>">
							</div>
						</div>
						<div class="form-group row">
							<label  class="col-sm-2 col-form-label">Descrição</label>
							<div class="col-sm-10">
								<textarea class="form-control" name="descricao" rows="3"> <?php echo $resultado_array[0]['descricao']; ?> </textarea>
							</div>
						</div>
						<!-- <div class="form-group row">
							<label for="inputPassword" class="col-sm-2 col-form-label">Kcal</label>
							<div class="col-sm-10">
								<input type="number" class="form-control" id="kcal" name="kcal">
							</div>
						</div> -->
						<h6>Categoria:</h6>
				<?php	if($resultado_array[0]['vegano']==1){ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria1" value="1" checked>
									<label class="form-check-label">Vegana</label>
								</div>
				<?php	}else{ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria1" value="1" >
									<label class="form-check-label">Vegana</label>
								</div>
				<?php }	?>
				<?php if($resultado_array[0]['vegetariano']==1){ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria2"  value="2" checked>
									<label class="form-check-label">Vegetariana</label>
								</div>
				<?php	}else{ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria2"  value="2">
									<label class="form-check-label">Vegetariana</label>
								</div>
				<?php } ?>
				<?php if($resultado_array[0]['sem_lactose']==1){ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria3"  value="3" checked>
									<label class="form-check-label">Sem lactose</label>
								</div>
				<?php	}else{ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria3"  value="3">
									<label class="form-check-label">Sem lactose</label>
								</div>
				<?php } ?>
				<?php if($resultado_array[0]['sem_gluten']==1){ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria4"  value="4" checked>
									<label class="form-check-label">Sem gluten</label>
								</div>
				<?php	}else{ ?>
								<div class="form-check">
										<input class="form-check-input" type="checkbox" name="categoria4"  value="4" >
										<label class="form-check-label">Sem gluten</label>
								</div>
				<?php } ?>
				<?php if($resultado_array[0]['sem_acucar']==1){ ?>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" name="categoria5"  value="5" checked>
									<label class="form-check-label">Sem açúcar</label>
								</div>
				<?php	}else{ ?>
								<div class="form-check">
										<input class="form-check-input" type="checkbox" name="categoria5"  value="5">
										<label class="form-check-label">Sem açúcar</label>
								</div>
				<?php } ?>
						<p> <br> </p>
						<div class="row">
							<div class="col col-12 col-md-6 offset-md-3">
								<div class="form-group">
									<p>Selecione o arquivo</p>
									<label class="custom-file">
										<span class="fake-button">
											Escolher arquivo
											<svg aria-hidden="true" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-upload fa-w-16 fa-lg"><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z" class=""></path></svg>	
										</span>

										<input type="file" class="form-control-file" id="img">
									</label>
								</div>
						<div class="">
							<input type="submit" class="btn btn-primary btn-lg btn-block" value="Pronto!" name="edita_receita_vdd">
						</div>
					</form>
				</div>	
			</div>
		</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>		
	</body>

<?php
}else{
  header('Location: index.php');
}
?>
</html>	