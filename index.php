<?php
	require_once('assets/inc-php/func.php');

	$sql="SELECT * FROM receitas";
	$resultado = pg_query($conexao, $sql);
	$resultado_array = pg_fetch_all($resultado);
?>
<!DOCTYPE html>
<html>
<head>
	<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="C:\Bitnami\wappstack-7.1.17-0\apache2\htdocs\real_meal\assets\js/script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">	
	<title>Real Meal | Home</title>
</head>
<body>
<?php
	if(isset($_SESSION['id'])){ //se está logado
?>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<img src="assets/img/real_meal.png" >
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Alimentos<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="perfil.php">Perfil<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="dash.php">Dash<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<form name="formsair" action="assets/inc-php/func.php" method="post">
						<input type="submit" class="btn btn-outline-secondary btn-sm" value="Sair" name="sair">
					</form>
				</li>
			</ul>
		</div>
	</nav>
<?php }else { ?>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">
			<img src="assets/img/real_meal.png" >
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Alimentos<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="login.php">Login<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="adc_user.php">Novo usuário<span class="sr-only"></span></a>
				</li>
			</ul>
		</div>
	</nav>
<?php } ?>
<main>
	<section class="filters py-3">
		<div class="container">
			<ul class="text-center">
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="#vegan" class="filter-link btn btn-light" id="f_vegano">Vegano</a>
				</li>
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="#vegie" class="filter-link btn  btn-light" id="f_vegetariano">Vegetariano</a>
				</li>
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="#gluten-free" class="filter-link btn btn-light" id="f_gluten">Gluten Free</a>
				</li>
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="#lactose-free" class="filter-link  btn btn-light" id="f_lactose">Lactose free</a>
				</li>
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="#sugar-free" class="filter-link btn btn-light" id="f_sugar">Sugar Free</a>
				</li>
			</ul>
		</div>
	</section>
	<section class="news py-4">
		<div class="container">
			<div class="row">
				<div class="col col-12 col-md-10 offset-md-1">
					<ul class="news-list row">
						<?php foreach($resultado_array as $conteudo){ ?>
							<li class="news-item col-12 col-md-4 my-2">
								<article class="news-list--item card">
									<img class="card-img-top" src="assets/img/receitas/<?php echo $conteudo['imagem'] ?>" alt="Imagem receita">
									<div class="card-body">
										<h5 class="card-title"><?php echo $conteudo['titulo']?></h5>
										<p class="card-text">
											<?php if($conteudo['vegano']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/vegano_verde.png" alt="Icone opção vegana">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/vegano_preto.png" alt="Icone opção não vegana">
											<?php } ?>
											<?php if($conteudo['vegetariano']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/vegetariano_verde.png" alt="Icone opção vegetariana">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/vegetariano_preto.png" alt="Icone opção não vegetariana">
											<?php } ?>
											<?php if($conteudo['sem_lactose']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/lactose_verde.png" alt="Icone opção sem lactose">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/lactose_preto.png" alt="Icone opção com lactose">
											<?php } ?>
											<?php if($conteudo['sem_gluten']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/gluten_verde.png" alt="Icone opção sem gluten">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/gluten_preto.png" alt="Icone opção com gluten">
											<?php } ?>
											<?php if($conteudo['sem_acucar']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/acucar_verde.png" alt="Icone opção sem açúcar">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/acucar_preto.png" alt="Icone opção com açúcar">
											<?php } ?>
										</p>
										<a href="#">Ler mais</a>
									</div>
								</article>	
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
</main>
	<!-- <div class="container filtros">
		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><h5>Gluten Free</h5></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><h5>Lactose Free</h5></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><h5>Sugar Free</h5></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><h5>Vegetarian</h5></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><h5>Vegan</h5></a>
			</li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">...</div>
			<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
			<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
		</div>		
	</div>
	<div class="jumbotron" align="center">
		<div class="card" style="width: 50rem;">
			<h4 class="card-header">Minhas receitas</h4>
			<div class="card-body">
				<table class="table">
					<tbody>
						<?php
							foreach($resultado_array as $conteudo){
								$html = 
								'<tr>
									<td>'.$conteudo['titulo'].'</td>
								</tr>';
								echo $html;
							}	
						?>
						</tr>
					</tbody>
				</table>
				<a class="btn btn-primary" href="nova_receita.php">Nova receita</a>
			</div>
		</div>
	</div> -->
	<script src="./assets/js/jquery-3.3.1.min.js"></script>
	<script src="./assets/js/bootstrap.min.js"></script>
	<script src="./assets/js/scripts.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>