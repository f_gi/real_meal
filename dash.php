<?php
require_once('assets/inc-php/func.php');
if(isset($_SESSION['id'])){ //só pra ver se tem alguém logado
	$id=$_SESSION['id'];
	$sql="SELECT * FROM receitas WHERE id_usuario=$id";
	$resultado = pg_query($conexao, $sql);
	$resultado_array = pg_fetch_all($resultado);
?>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript" src="assets/js/script.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">	
	<title>Real Meal | Minhas receitas</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="index.php">
			<img src="assets/img/real_meal.png" >
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="#">Alimentos<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="perfil.php">Perfil<span class="sr-only"></span></a>
				</li>
				<li class="nav-item active">
					<form name="formsair" action="assets/inc-php/func.php" method="post">
						<input type="submit" class="btn btn-outline-secondary btn-sm" value="Sair" name="sair">
					</form>
				</li>				
			</ul>
		</div>
	</nav>
	<section class="filters py-3">
		<div class="container">
			<ul class="text-center">
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<h4>Minhas receitas</h4> 
				</li>
				<li class="filter my-3 my-md-0 d-block d-md-inline-block">
					<a href="nova_receita.php" class="filter-link btn btn-light n-rec"><h6>Nova receita</h6></a>
				</li>
			</ul>
		</div>
	</section>
	<section class="news py-4">
		<div class="container">
			<div class="row">
				<div class="col col-12 col-md-10 offset-md-1">
					<ul class="news-list row">
						<?php foreach($resultado_array as $conteudo){ ?>
							<li class="news-item col-12 col-md-4 my-2">
								<article class="news-list--item card">
									<img class="card-img-top" src="assets/img/receitas/<?php echo $conteudo['imagem'] ?>" alt="Imagem receita">
									<div class="card-body">
										<h5 class="card-title"><?php echo $conteudo['titulo']?></h5>
										<p class="card-text">
											<?php if($conteudo['vegano']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/vegano_verde.png" alt="Icone opção vegana">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/vegano_preto.png" alt="Icone opção não vegana">
											<?php } ?>
											<?php if($conteudo['vegetariano']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/vegetariano_verde.png" alt="Icone opção vegetariana">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/vegetariano_preto.png" alt="Icone opção não vegetariana">
											<?php } ?>
											<?php if($conteudo['sem_lactose']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/lactose_verde.png" alt="Icone opção sem lactose">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/lactose_preto.png" alt="Icone opção com lactose">
											<?php } ?>
											<?php if($conteudo['sem_gluten']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/gluten_verde.png" alt="Icone opção sem gluten">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/gluten_preto.png" alt="Icone opção com gluten">
											<?php } ?>
											<?php if($conteudo['sem_acucar']==1){ ?>
												<img class="icone_receita" src="assets/img/icones/acucar_verde.png" alt="Icone opção sem açúcar">
											<?php }else{ ?>
												<img class="icone_receita" src="assets/img/icones/acucar_preto.png" alt="Icone opção com açúcar">
											<?php } ?>
										</p>
										<a href="#">Ler mais</a>
										<form name="" action="assets/inc-php/func.php" method="post" class="form_dash">
											<input type="number" name="id_receita" class="esconder" value="<?php echo $conteudo['id'];?>">
											<input type="submit" class="btn btn-outline-primary btn-sm" value="Editar" name="editar_receita">
											<input type="submit" class="btn btn-outline-secondary btn-sm" value="Excluir" name="excluir_receita">
										</form>
									</div>
								</article>	
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</section>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
<?php
}else{
	header('Location: index.php');
}
?>